---
layout: handbook-page-toc
title: "TPRM AppSec Review Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

A <abbr title="Third-Party Risk Management">TPRM</abbr> application security review is needed when
GitLab adopts usage of third-party services and products which handle [red data](https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#red). The main focus
of a TPRM review should be to highlight anything that helps evaluate whether adopting the product or
service would introduce any new risk to GitLab and our data. This involves validating the scope of
information collected and how it's transmitted and handled but also includes more traditional
application security review aspects, such as identifying potential vulnerabilities, security
best-practice violations, and assessing the general security maturity and posture of the product or
service.

### How TPRM AppSec reviews are kicked off?

These reviews are typically kicked off by our third-party risk team by using the [TPRM AppSec review issue template](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-reviews/-/issues/new?issue%5Bmilestone_id%5D=#) (internal link). Though anyone at GitLab is welcome to request a AppSec TPRM review by utilizing the above template.

### Scope definition

Before reviewing, think about the scope you want this review to cover. It's important to communicate
what was, and was not, covered in the review, and it gives you a rough plan on what you need to do.
A scope can be defined with different components, like backend API, iOS App, Browser extension,
or by key features such as Document Upload, Slack Integration, Single Sign-On, etc.

### Traffic capturing with Burp Suite

Always proxy traffic through [Burp Suite] when reviewing a product or service. Burp will capture all
traffic going over the wire which will make validating the scope of information collected much
easier and low-hanging security vulnerabilities and concerns will be passively detected by Burp's
built-in scanners and extensions. Make sure to also save the project to disk! This makes it easier
to answer any potential questions after the review is done.

#### Recommended Burp Suite extensions:

The following extensions enhances Burp Suite's passive detection capabilities which can give
valuable insights into the target's general security maturity:

- `retire.js`: Detects vulnerable JavaScript libraries in use
- `WAFDetect`: Detects presence of Web Application Firewalls
- `CSPAuditor`: Detects weak Content Security Policy configurations
- `Hunt Scanner`: Logs requests containing parameters that are often vulnerable to various attacks

See documentation on [installing Burp Suite extensions].

## Assessing the product

#### Core features and functionality

Explore the core features and functionality of the product in your Burp Suite intercepted browser.
This should include sign-up, sign-in, sign-out, settings, creating and deleting things, and anything
else that are considered core features and functionality. Notice anything that pops out to you as
being "weird" or insecure.

Perhaps data is being transmitted over plaintext HTTP, or maybe password re-entry is not required
when changing your email, or maybe your session token is still valid after signing out? Keep in mind
though, that this is not a full-scale penetration test, so stick to passive observation and smaller
checks that don't require a lot of time, but would give you an idea of the product's general
security maturity and posture.

Apart from technical issues, also consider whether a feature requires an excessive amount of
information or access compared to what it is supposed to do. Ask yourself if you would be
comfortable with the usage of the feature as:

- **Yourself:** would you, yourself, be comfortable using this feature?
- **A GitLab team member:** would you, as a GitLab team member, be comfortable with this feature being
used by other GitLab team members?
- **A GitLab user:** would you, as a user of GitLab, be comfortable if you knew this feature was
used at GitLab?

If any of these questions are answered with **No**, write it down as a finding or concern.

#### Authentication

How does the product do authentication? Does it support Single Sign-On with Okta? If not, does it
support other login providers, or at least support Multi-Factor authentication?

#### Access Control

Does the product support robust access control through permission roles or access groups?

#### Other security features

What other security features does the product support that could help us secure the use of it? Does
it have a detailed audit log of activity or email notifications on important events?

#### Integrations

If the product supports any sort of integration with other systems, e.g. Google Workspace, Slack, or
Snowflake, how does the product gain access to these systems, and what will it do with this access?

- Are permission scopes excessive compared to what the product does?
- Can the type, and amount of data, collected be controlled by GitLab?
- Can we monitor for unauthorized usage or abuse from this access?
- Can we easily, and quickly, deny the access in case of abuse?

#### Infrastructure

Look through the target site map and proxy log in Burp Suite and note down all the unique hosts
involved in serving the product. This includes frontend servers, API hosts,
Analytics/Metrics collection, and CDN hosts.

- Are they hosted in the Cloud, or "in-house" under the company's network range?
- What kind of data, and how much, is collected by analytics and metrics systems?
- Is the CDN provider well-known and robust?
- How well is TLS configured on these hosts? Check with Qualys [SSL Labs].

#### Passive Findings

When you feel the product has been sufficiently explored, scroll through the findings from Burp
Suite's passive scanner and note anything that is worth mentioning as a finding or concern.

 - Are important security headers missing?
 - Any outdated and vulnerable JavaScript libraries in use?
 - Missing anti-CSRF tokens in forms?
 - Was a <abbr title="Web Application Firewall">WAF</abbr> detected? (this would obviously be a
 positive)

If you have the Hunt Scanner extension installed, go through the requests logged by the scanner and
see if any of the parameters look particularly vulnerable to an attack. If approval to perform a
security test has been given, feel free to test whether the endpoint is vulnerable or not, but keep
it brief and be careful not to cause any disruption or violate other user's privacy!

### Documenting findings

Each finding or security concern should get its own thread in the review issue. The following
template can be used:

```markdown
### short description
** Severity: Informational/Low/Medium/High/Critical **

details and reproduction steps
```

The heading should contain a concise description of the issue. For instance "Reflected Cross-Site
Scripting in search parameter" or "Lack of certificate validation in backend communication". The
details should go in-depth such that the issue is clearly understandable and, if applicable, can be
reproduced with reasonable effort.

[Burp Suite]: https://portswigger.net/burp
[installing Burp Suite extensions]: https://portswigger.net/support/how-to-install-an-extension-in-burp-suite
[SOC 2]: https://www.imperva.com/learn/data-security/soc-2-compliance/
[ISO/IEC 27001]: https://en.wikipedia.org/wiki/ISO/IEC_27001
[SSL Labs]: https://www.ssllabs.com/
